package uk.co.couponstore.api.v1

import java.io.File

import akka.actor.{ActorRefFactory, Actor}
import org.parboiled.common.FileUtils
import spray.can.Http
import spray.can.server.Stats
import spray.http._
import spray.httpx.encoding.Gzip
import spray.httpx.marshalling.Marshaller
import spray.json._
import spray.routing.HttpService


/**
 * We don't implement out route structure directly in the servuce actor because we want to be
 * able to test it independently, without having to spin up an actor.
 */
class RestInterfaceActor extends Actor with RestApi {

  // the HttpService trait defines only one abstract member, which
  // connects the service environment to the enclosing actor or test
  def actorRefFactory: ActorRefFactory = context

  // this actor only runs one route, but you could add
  // other things here, like request stream processing,
  // timeout handling or alternative handle registration
  override def receive: Receive = runRoute(routes)

}


import scala.concurrent.duration._
import akka.pattern.ask

trait RestApi extends HttpService with CodeService {

  // we use the enclosing ActorContext's or ActorSystem's dispatcher for our Futures and Scheduler
  implicit  def executionContext = actorRefFactory.dispatcher


  val routes = {

    get {
      pathSingleSlash {
        complete(index)
      } ~
        path("ping") {
          complete("Pong!")
        } ~
        path("stream-large-file") {
          encodeResponse(Gzip) {
            getFromFile(largeTempFile)
          }
        } ~
        path("stats") {
          complete {
            actorRefFactory.actorFor("/user/IO-HTTP/listener-0")
              .ask(Http.GetStats)(1.second)
              .mapTo[Stats]
          }
        } ~
        pathPrefix("generate") {
          path("code") {
            complete(generate())
          } ~
            path("codes") {
              respondWithMediaType(MediaTypes.`text/plain`) {
                respondWithHeader(HttpHeaders.`Content-Disposition`.apply("attachment", Map("filename" -> "codes.csv"))) {
                  val codes:Seq[String] = generateMany(100)
                  val file = File.createTempFile("codes", ".csv")
                  FileUtils.writeAllText(codes mkString("\n"), file)
                  file.deleteOnExit()
                  getFromFile(file)
                }
              }
            }
        }
    } ~
      put {
        pathPrefix("generate") {
          path("codes") {
            formFields('color, 'age.as[Int]) { (color, age) =>
              complete(s"The color is '$color' and the age ten years ago was ${age - 10}")
            }
          }
        }

      } ~
      post {
        path("person") {

          import PersonJsonSupport._
          entity(as[Person]) { person =>
            complete(s"Person: ${person.name} - favorite number: ${person.favoriteNumber}")
          }
        }
      }

  }

  implicit val statsMarshaller: Marshaller[Stats] =
    Marshaller.delegate[Stats, String](ContentTypes.`text/plain`) { stats =>
      "Uptime : " + stats.uptime + '\n' +
        "Total requests : " + stats.totalRequests + '\n' +
        "Open requests : " + stats.openRequests + '\n' +
        "Max open requests : " + stats.maxOpenRequests + '\n' +
        "Total connections : " + stats.totalConnections + '\n' +
        "Open connections : " + stats.openConnections + '\n' +
        "Max open connections : " + stats.maxOpenConnections + '\n' +
        "Requests timed out : " + stats.requestTimeouts + '\n'
    }

  lazy val index =
    <html>
      <head>
        <title>Code Generating API</title>
      </head>
      <body>
        <h1>Code Generator using <i>spray-routing</i> on <i>spray-can</i>!</h1>
        <ul>
          <li><a href="/ping">/ping</a></li>
          <li><a href="/generate/code">/generate/code</a></li>
          <li><a href="/stream1">/stream1</a> (via a Stream[T])</li>
          <li><a href="/stream2">/stream2</a> (manually)</li>
          <li><a href="/stream-large-file">/stream-large-file</a></li>
          <li><a href="/stats">/stats</a></li>
          <li><a href="/timeout">/timeout</a></li>
          <li><a href="/cached">/cached</a></li>
          <li><a href="/crash">/crash</a></li>
          <li><a href="/fail">/fail</a></li>
          <li><a href="/stop?method=post">/stop</a></li>
        </ul>
      </body>
    </html>

  lazy val largeTempFile: File = {
    val file = File.createTempFile("streamingTest", ".txt")
    FileUtils.writeAllText((1 to 1000).map("This is line " + _) mkString "\n", file)
    file.deleteOnExit()
    file
  }

}


