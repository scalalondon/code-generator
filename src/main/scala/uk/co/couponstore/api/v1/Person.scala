package uk.co.couponstore.api.v1

import spray.json.DefaultJsonProtocol

case class Person(name: String, favoriteNumber: Int)

object PersonJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val PersonFormat = jsonFormat2(Person)
}
