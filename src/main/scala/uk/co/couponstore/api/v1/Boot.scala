package uk.co.couponstore.api.v1

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.typesafe.config.ConfigFactory
import spray.can.Http

/**
 * Start a Spray Can Server (Build on Akka Actor Toolkit).
 */
object Boot extends App {

  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  // we need an actor system to host our application on
  implicit val system = ActorSystem("code-generator")

  // create and start our service actor
  val api = system.actorOf(Props[RestInterfaceActor], "rest-interface")

  // start the new HTTP service on port and host from configuration file. Hook into the Actor system.
  IO(Http) ! Http.Bind(listener = api, interface = host, port = port)
}



