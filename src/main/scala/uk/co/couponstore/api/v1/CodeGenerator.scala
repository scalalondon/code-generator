package uk.co.couponstore.api.v1

trait CodeService {

  val upperAlphaNumericChars = ('A' to 'Z') ++ ('0' to '9')
  val blockedChars = Seq('I','O','1','0')
  val charsForCodeGen = upperAlphaNumericChars.filterNot(blockedChars.contains(_))

  /**
   * Generate a Random Code.
   *
   * @param prefix
   * @param suffix
   * @return
   */
  def generate(prefix: String = "", suffix: String = "", segmentLength: Integer = 6, delimiter: String = "-", segments: Integer = 2): String = {

    def loop(segmentNumber: Integer, codeSoFar: String): String =  {
      if(segmentNumber == segments) return codeSoFar
      else loop(segmentNumber + 1, codeSoFar + delimiter + randomStringFromCharList(segmentLength, charsForCodeGen))
    }
    val generatedString = loop(1,randomStringFromCharList(segmentLength-prefix.length,charsForCodeGen))
    if(suffix.length>0) prefix + generatedString.substring(0, generatedString.length - suffix.length) + suffix
    else prefix + generatedString
  }

  /**
   *
   * @param length
   * @param chars
   * @return
   */
  private def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
    val sb = new StringBuilder
    for (i <- 1 to length) {
      val randomNum = util.Random.nextInt(chars.length)
      sb.append(chars(randomNum))
    }
    sb.toString
  }

  /**
   * Generate Many Codes.
   *
   * @param quantity
   * @param prefix
   * @param suffix
   * @param segmentLength
   * @param delimiter
   * @param segments
   * @return
   */
  def generateMany(quantity: Integer, prefix: String = "", suffix: String = "", segmentLength: Integer = 6, delimiter: String = "-", segments: Integer = 2): Seq[String] = {
    for(x <- 1 to quantity) yield generate(prefix, suffix,segmentLength,delimiter,segments)
  }


}