package uk.co.couponstore.api.v1

import akka.actor.ActorRefFactory
import org.scalatest.{FlatSpec, Matchers}
import org.slf4j.LoggerFactory
import spray.http._
import spray.testkit.ScalatestRouteTest
import spray.testkit.TestUtils
import spray.routing.HttpService
import spray.http.StatusCodes._
import spray.httpx.SprayJsonSupport._

import spray.routing.{ MethodRejection, RequestContext, Directives }

/**
 * https://gagnechris.wordpress.com/2013/09/15/building-restful-apis-with-scala-using-spray/
 * http://github.com/spray/spray/tree/release/1.2/spray-routing-tests/src/test/scala/spray/routing
 */
class CodeGeneratorHttpSpec extends FlatSpec with Matchers with Directives with ScalatestRouteTest with RestApi {

  override def actorRefFactory: ActorRefFactory = system

  "Route" should "display menu items for /" in {
    Get("/") ~> routes ~> check {
      status should equal(OK)
      info(s"ContentType: $contentType")
      ContentType(MediaTypes.`text/xml`)
      contentType.toString() should equal("text/xml; charset=UTF-8")
    }
  }

  it should "return pong for /ping" in {
    Get("/ping") ~> routes ~> check {
      status should equal(OK)
      info(s"ContentType: $contentType")
      ContentType(MediaTypes.`text/plain`)
      contentType.toString() should equal("text/plain; charset=UTF-8")
      body.data.asString(HttpCharsets.`UTF-8`)  should equal("Pong!")
    }
  }

  it should "accept a PUT for /generate/codes" in {
    Put("/generate/codes", FormData(Seq("color" -> "blue", "age" -> "68"))) ~> routes ~> check {
      info(s"Response: ${responseAs[String]}")
      responseAs[String] should equal("The color is 'blue' and the age ten years ago was 58")
    }
  }

  it should "fail for a POST to /generate/codes " in {
    Post("/generate/codes") ~> sealRoute(routes) ~> check {
      status === StatusCodes.BadRequest
      info(s"Response: ${responseAs[String]}")
      responseAs[String] should equal("The requested resource could not be found.")
    }
  }

  it should "accept a POST with JSON for /person" in {
     Post("/person", HttpEntity(MediaTypes.`application/json`, """{ "name": "Jane", "favoriteNumber" : 42 }""" )) ~>
      routes ~> check {
      responseAs[String] should equal("Person: Jane - favorite number: 42")
    }
  }

}

