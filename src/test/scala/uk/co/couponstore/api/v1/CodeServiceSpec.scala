package uk.co.couponstore.api.v1

import org.scalatest.{FlatSpec, Matchers}
import org.slf4j.{Logger, LoggerFactory}

/**
 * Test Code Generator.
 */
class CodeServiceSpec extends FlatSpec with Matchers with CodeService {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  "Generate Code" should "should not contain 1, I, O or 0" in {
    // logger.debug(s"charsForCodeGen=$charsForCodeGen, type=${charsForCodeGen.getClass}")
    charsForCodeGen should contain theSameElementsAs (charsForCodeGen)
    charsForCodeGen should contain noneOf ('I','O','1','0')
  }

  it should "generate default return a code matching the regex [A-Z2-9]{6}-[A-Z2-9]{6}" in {
    val result: String = generate()
    result should fullyMatch regex("[A-Z2-9]{6}-[A-Z2-9]{6}")
    result should fullyMatch regex("[^I10]{13}")
  }

  it should "generate with a prefix return a code matching the regex WOW[A-Z2-9]{3}-[A-Z2-9]{6}" in {
    val result: String = generate(prefix = "WOW")
    logger.info(result)
    result should fullyMatch regex("WOW[A-Z2-9]{3}-[A-Z2-9]{6}")
    result should startWith("WOW")
    result should fullyMatch regex("WOW[^IO10]{10}")
  }

  it should "generate with a suffix and return a code matching the regex [A-Z2-9]{6}-[A-Z2-9]{3}123" in {
    val result: String = generate(suffix = "123")
    logger.info(result)
    result should fullyMatch regex("[A-Z2-9]{6}-[A-Z2-9]{3}123")
    result should endWith regex("123")
  }

  it should "generate with prefix, suffix, length 12, 1 Segment" in {
    val result: String = generate(prefix = "WOW", suffix = "123", segmentLength = 12, delimiter = "", segments = 1)
    logger.info(result)
    result should fullyMatch regex("WOW[A-Z2-9]{3}[A-Z2-9]{3}123")
    result should fullyMatch regex("WOW[^IO10]{3}[^IO10]{3}123")

  }

  it should "generate a code with 4 segments of each with length 3" in {
    val result: String = generate(segmentLength = 3, delimiter = "-", segments = 4)
    logger.info(result)
    result should fullyMatch regex("[A-Z2-9]{3}-[A-Z2-9]{3}-[A-Z2-9]{3}-[A-Z2-9]{3}")
    result should fullyMatch regex("[^IO10]{3}-[^IO10]{3}-[^IO10]{3}-[^IO10]{3}")
  }

  it should "generate code with prefix, puffix, 4 segments of each with length 3" in {
    val result: String = generate(prefix = "WOW", suffix = "123", segmentLength = 3, delimiter = "-", segments = 4)
    logger.info(result)
    result should fullyMatch regex("WOW-[A-Z2-9]{3}-[A-Z2-9]{3}-123")
    result should fullyMatch regex("WOW-[^IO10]{3}-[^IO10]{3}-123")
  }

  "Generate many codes" should "be successful" in {
    val codes = generateMany(2)
    codes should have size(2)
  }

}
